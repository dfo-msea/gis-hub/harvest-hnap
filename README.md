# Title of project

__Main author:__  Cole Fields  
__Affiliation:__  Fisheries and Oceans Canada (DFO)   
__Group:__        Marine Spatial Ecology and Analysis   
__Location:__     Institute of Ocean Sciences   
__Contact:__      e-mail: Cole.Fields@dfo-mpo.gc.ca | tel:


- [Objective](#objective)
- [Summary](#summary)
- [Status](#status)
- [Contents](#contents)
- [Methods](#methods)
- [Requirements](#requirements)
- [Caveats](#caveats)
- [Acknowledgements](#acknowledgements)


## Objective
The main goal of this project is to download HNAP-compliant XML files from https://gis-hub.ca for harvesting on the EDH.


## Summary
This script is designed to download the XML files for all available datasets by default. However, it can also be specified to download a single dataset by its name or ID. The script makes use of Python's built-in logging module to log events, which are saved in a log file in the /logs directory. Metadata xml files will be downloaded to a /xml-files/<datestamp> subdirectory


## Status
In-development


## Contents
The script contains several functions to send requests, filter datasets, get datasets, extract dataset names, and download HNAP XML data.


## Methods
The script uses the requests library to send HTTP requests to the GIS Hub API. It uses the logging module to log events. The script includes error handling for failed requests and unexpected errors. If a request fails, the script logs the error and returns a default error message. If an unexpected error occurs, the script logs the error and the traceback, and returns a default error message. If the script encounters an error while downloading an HNAP file, it logs the error and continues with the next file.


## Requirements
Before running the script, create a Python virtual environment for this project. Create a new environment: `python -m venv harvest-hnap` and activate it. Once the new environment is activated, run `pip install -r requirements.txt` to install the required packages. Also, make sure to set up the environment variable GISHUB_TOKEN with your API token. This can be done by creating a .env file with a GISHUB_TOKEN value.


## Caveats
The script ,ust be run within a Python environment that has the `dotenv` and `requests` packages installed. The script assumes that the API token is stored in the GISHUB_TOKEN environment variable. If this variable is not set, the script will exit with an error message. The script also assumes that the GIS Hub API is available and responds to requests in a timely manner.


## Acknowledgements
This script was developed within Fisheries and Oceans Canada (DFO) Marine Spatial Ecology and Analysis section.
