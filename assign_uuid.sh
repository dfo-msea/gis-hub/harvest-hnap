#!/bin/bash

# Directory containing the XML files
dir_path="D:\\projects\\gis-hub\\harvest-hnap\\xml-files\\20240426_110827"

# Strings to replace with
replace1='<?xml version="1.0" encoding="UTF-8"?>'
replace2='\\    <gco:CharacterString>a722883a-1a4e-4523-9b9f-ae0c9c7e2797</gco:CharacterString>'

# Use find to get all XML files in the directory, and sed to perform the replacements
find "$dir_path" -name '*.xml' -exec sed -i.bak -e "1c$replace1" -e "4c$replace2" {} \;
