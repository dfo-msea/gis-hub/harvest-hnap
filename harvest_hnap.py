import settings
import argparse
import os
import sys
import requests
import traceback


settings.setup_folders()
logger = settings.setup_logger()


try:
    GISHUB_TOKEN = os.environ['GISHUB_TOKEN']
    GHUB_HEADERS = {'Authorization': GISHUB_TOKEN}
    logger.info('API token loaded successfully.')
except KeyError:
    logger.error('Environment variable GISHUB_TOKEN not found! Make sure a .env file exists with a GISHUB_TOKEN value.')
    sys.exit(1)


def make_request(url, xml=False):
    """ Send a request to URL using authentication headers and return the response or a json object of the response. """
    try:
        response = requests.get(url, headers=GHUB_HEADERS)
    except requests.RequestException as e:
        logger.error(f'Request to {url} failed: {e}')
        logger.error(traceback.format_exc())
        return settings.DEFAULT_ERROR
    except Exception as e:
        logger.error(f'An unexpected error occurred: {e}')
        logger.error(traceback.format_exc())
        return settings.DEFAULT_ERROR
    status_code = response.status_code
    logger.debug(f'Status code: {status_code}')
    if 200 <= status_code < 300:
        return response.json() if not xml else response
    else:
        logger.warning(f'Error {status_code}')
        error_messages = {
            400: 'Bad request.',
            401: 'Unauthorized.',
            403: 'Forbidden.',
            404: 'Not found.',
            408: 'Request timeout.',
            409: 'Data conflict.',
            500: 'Internal server error.'
        }
        message = error_messages.get(status_code, f'Unknown error occurred for URL: {url}')
        logger.warning(message)
        try:
            json_resp = response.json()
            logger.warning(json_resp)
            return json_resp
        except ValueError:
            if response.reason:
                settings.DEFAULT_ERROR['reason'] = response.reason
                logger.error(f'Reason: {response.reason}')
            return settings.DEFAULT_ERROR


def is_shareable(dataset_metadata):
    """ Given a data dictionary, return whether the values from status and private fields are OK for inclusion in filtered list. """
    dataset_status = dataset_metadata.get('status')
    is_private = dataset_metadata.get('private')
    if not dataset_status:
        logger.error(f'{dataset_metadata.get("name")} does not have a value for "status" field.')
        return False
    if is_private is None:
        logger.error(f'{dataset_metadata.get("name")} does not have a value for "private" field.')
        return False
    return dataset_status.lower() not in ['proposed', 'under development'] and not is_private


def filter_datasets(metadata_list):
    """ Return filtered list of datasets, removing entries based on condition. 
        The token will limit results to certain datasets, but may still need filtering.
    """
    num_datasets = len(metadata_list)
    filtered_list = []
    logger.info(f'Filtering list of {num_datasets} datasets based on condition.')
    for data_dict in metadata_list:
        dataset_name = data_dict.get('name')
        if dataset_name in settings.SKIP:
            logger.warning(f'{dataset_name} (skipped - likely a test record): included [x]')
        elif is_shareable(data_dict):
            logger.info(f'{dataset_name}: included [\u2713]')
            filtered_list.append(data_dict)
        else:
            logger.warning(f'{dataset_name} (skipped - Private or status is Proposed/Under Development): included [x]')
    excluded = num_datasets - len(filtered_list)
    logger.info(f'{excluded} datasets excluded.')
    return filtered_list


def get_datasets(id=None):
    """ Return list of metadata entries from GIS Hub for a all datasets (default) based on an FQ query on the server or for a single entry by name/id. """
    if id:
        logger.info(f'Requesting {id} dataset metadata from GIS Hub.')
        resp = make_request(settings.PACKAGE_SHOW + id)
        if not resp.get('success'):
            logger.error(f'Request for {id} failed. {id} may not be a valid GIS Hub dataset.')
            sys.exit()
        results = resp.get('result', {})
        logger.info('Request OK')
        return [results]
    else:
        logger.info(f'Requesting all dataset metadata from GIS Hub.')
        resp = make_request(settings.PACKAGE_SEARCH)
        results = resp.get('result', {}).get('results', [])
        if not isinstance(results, list):
            logger.error(f'List of datasets (results) is {type(results)}, expected a list')
            sys.exit()
        return results


def get_names(metadata_list):
    """ Return list of dataset names given a list of dictionaries. """
    logger.info('Extracting dataset names from metadata entries.')
    return [record.get('name') for record in metadata_list]


def download_hnap(name_list):
    """ Given a list of dataset names, download the HNAP xml data and write to local disk on local machine. """
    logger.info(f'Starting download of {len(name_list)} HNAP xml files.')
    for i, dataset_name in enumerate(name_list, start=1):
        hnap_url = settings.DATASET_URL + f'/{dataset_name}' + settings.HNAP
        resp = make_request(hnap_url, xml=True)
        if isinstance(resp, dict):
            logger.warning(f'Failed to download HNAP for {dataset_name}. Reason: {resp.get("reason", "Unknown")}')
            logger.warning(f'{i:03} - {dataset_name}: download [x]')
            continue
        if resp.status_code == 200:
            hnap_file = os.path.join(settings.XMLSUBDIR, f'{dataset_name}.xml')
            with open(hnap_file, 'w', encoding=settings.ENCODING) as f:
                f.write(resp.text)
            logger.info(f'{i:03} - {dataset_name}: downloaded [\u2713]')


def main(args):
    """ Process HNAP file download for either a single dataset (args.name) or all datasets (default). """
    dataset_metadata = get_datasets(args.name)
    filtered_metadata = filter_datasets(dataset_metadata)
    dataset_names = get_names(filtered_metadata)
    download_hnap(dataset_names)


if __name__ == '__main__':
    description = 'Download HNAP-compliant xml file(s) from https://gis-hub.ca for harvesting on the EDH. ' \
                  'By default, the script will download the xml files for all available datasets.'
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('-n', '--name', type=str, help='Run on a single dataset by dataset name or ID.')
    args = parser.parse_args()
    main(args)
