#!/bin/bash

# Directory containing the XML files
directory="D:\\projects\\gis-hub\\scripts\\harvest-hnap\\xml-files\\20240430_100152"

# Iterate over each XML file in the directory - replace old metadata scope code
# FIRST
# find "$directory" -name '*.xml' -exec sed -i '14d;12d;' {} \;
# SECOND
# find "$directory" -name '*.xml' -exec sed -i '12s|.*|  <gmd:MD_ScopeCode codeListValue="RI_622" codeList="https://schemas.metadata.geo.ca/register/napMetadataRegister.xml#IC_108">dataset; jeuDonnées</gmd:MD_ScopeCode>|' {} \;



# Use find to locate XML files and modify them with sed
# find "$directory" -name '*.xml' -exec sed -i 's|<gmd:hierarchyLevel>\s*<gmd:MD_ScopeCode codeListValue="dataset" codeList="./resources/codeList.xml#MD_ScopeCode" />\s*</gmd:hierarchyLevel>|  <gmd:MD_ScopeCode codeListValue="RI_622" codeList="https://schemas.metadata.geo.ca/register/napMetadataRegister.xml#IC_108">dataset; jeuDonnées</gmd:MD_ScopeCode>|' {} \;

# # Strings to search for
# search1='<gmd:hierarchyLevel>'
# search2='<gmd:MD_ScopeCode codeListValue="dataset" codeList="./resources/codeList.xml#MD_ScopeCode" />''
# search3='</gmd:hierarchyLevel>'

# # Strings to replace with
# replace1=''
# replace2='https://schemas.metadata.geo.ca'
# replace3='<gmd:MD_ScopeCode codeListValue="dataset" codeList="./resources/codeList.xml#MD_ScopeCode" />'

# Use find to get all XML files in the directory, and sed to perform the replacements
# find "$dir_path" -name '*.xml' -exec sed -i "s|$search1|$replace1|g;s|$search2|$replace2|g" {} \;
