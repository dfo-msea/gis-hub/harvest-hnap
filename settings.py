from dotenv import load_dotenv
import os
import logging
from datetime import datetime

load_dotenv()
ENCODING = 'utf-8-sig'
BASE_URL = 'http://gis-hub.ca'
DATASET_URL = BASE_URL + '/dataset'
API_URL = BASE_URL + '/api/3/action'
LOG_NAME = 'HNAPHarvest'
LOG_FORMAT = logging.Formatter('%(asctime)s:%(levelname)s:%(module)s(%(lineno)04d) - %(message)s')
DATE_TIME = datetime.now().strftime("%Y%m%d_%H%M%S")
LOGDIR, XMLDIR = 'logs', 'xml-files'
XMLSUBDIR = os.path.join(XMLDIR, DATE_TIME)
PACKAGE_SEARCH = API_URL + '/package_search?include_private=True&rows=1000'
PACKAGE_SHOW = API_URL + '/package_show?id='
DEFAULT_ERROR = {'error': 'Server error'}
HNAP = '/download_hnap'
SKIP = ['meta-template',
        'test-05',
        'test-06',
        'fetch-resources',
        'goc-themes',
        'goc-thesaurus',
        'ia-documentation',
        'ia-documentation-pncima',
        'species-codes',
        'nsb-mpan-eco-ccira']


def setup_folders():
    """ Create folders for logs and xml files. """
    if not os.path.exists(LOGDIR):
        os.makedirs(LOGDIR)
    if not os.path.exists(XMLDIR):
        os.makedirs(XMLDIR)
    os.makedirs(XMLSUBDIR)


def get_file_logger():
    """ Return file handler and logfile path. """
    log_file = os.path.join('logs', f'{DATE_TIME}.log')
    fh = logging.FileHandler(log_file, encoding=ENCODING)
    return fh, log_file

def setup_logger(name=LOG_NAME, level=logging.INFO):
    logger = logging.getLogger(name)
    logger.setLevel(level)
    sh = logging.StreamHandler()
    sh.setFormatter(LOG_FORMAT)
    sh.setLevel(level)
    fh, log_file = get_file_logger()
    fh.setLevel(level)
    fh.setFormatter(LOG_FORMAT)
    # Remove existing handlers
    logger.handlers = []
    logger.addHandler(sh)
    logger.addHandler(fh)
    logger.debug(f'{name}: Logging to {log_file}')
    return logger
